package com.example.meuprimeiroapp

import android.content.res.ColorStateList
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var question: TextView
    private lateinit var option1: Button
    private lateinit var option2: Button
    private lateinit var option3: Button
    private lateinit var option4: Button

    private var questionList: List<Question> = emptyList()
    private var activeQuestion: Int = 0

    companion object {
        const val MAX_NUMBER_OPTIONS = 4
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViews()
        resetButtonsColor()

        setupQuestions()
        setQuestion(activeQuestion)

        // colocar a primeira questão

        // clicar o botão -> manda a resposta

        // compara a resposta

        // Se correta -> botão fica verde

        // se incorreta -> botão fica vermelho e a correta fica verde

        // muda de pergunta
    }

    private fun setupViews() {
        question = findViewById(R.id.question)
        option1 = findViewById(R.id.option1)
        option2 = findViewById(R.id.option2)
        option3 = findViewById(R.id.option3)
        option4 = findViewById(R.id.option4)

        option1.setOnClickListener { submitAnswer(0) }
        option2.setOnClickListener { submitAnswer(1) }
        option3.setOnClickListener { submitAnswer(2) }
        option4.setOnClickListener { submitAnswer(3) }
    }

    private fun submitAnswer(answerId: Int) {
        setButtonsEnabled(false)
        val currentQuestion = questionList[activeQuestion]
        val isCorrect = answerId == currentQuestion.correctAnswerId

        if (!isCorrect) setIncorrect(answerId)
        setCorrect(currentQuestion.correctAnswerId)

        Handler(Looper.getMainLooper()).postDelayed(
            {
                if (activeQuestion < questionList.size - 1) {
                    activeQuestion += 1
                    resetButtonsColor()
                    setQuestion(activeQuestion)
                }
            },
            1500,
        )
    }

    private fun setCorrect(answerId: Int) {
        when(answerId) {
            0 -> option1.setColorGreen()
            1 -> option2.setColorGreen()
            2 -> option3.setColorGreen()
            3 -> option4.setColorGreen()
        }
    }

    private fun setIncorrect(answerId: Int) {
        when(answerId) {
            0 -> option1.setColorRed()
            1 -> option2.setColorRed()
            2 -> option3.setColorRed()
            3 -> option4.setColorRed()
        }
    }

    private fun resetButtonsColor() {
        option1.setColorDarkBlue()
        option2.setColorDarkBlue()
        option3.setColorDarkBlue()
        option4.setColorDarkBlue()
    }

    private fun setupQuestions() {
        questionList = listOf(
            Question(
                getString(R.string.question_1),
                resources.getStringArray(R.array.options_1).toList(),
                3,
            ),
            Question(
                getString(R.string.question_2),
                resources.getStringArray(R.array.options_2).toList(),
                0,
            ),
            Question(
                getString(R.string.question_3),
                resources.getStringArray(R.array.options_3).toList(),
                2,
            ),
            Question(
                getString(R.string.question_4),
                resources.getStringArray(R.array.options_4).toList(),
                3,
            ),
        )
    }

    private fun setQuestion(questionId: Int) {
        if (questionId < questionList.size) {
            with(questionList[questionId]) {
                // Modificando o texto do enunciado
                question.text = this.questionText

                // Modificando o texto dos Botões
                option1.text = this.optionsList[0]
                option2.text = this.optionsList[1]
                option3.text = this.optionsList[2]
                option4.text = this.optionsList[3]
            }
        }
        setButtonsEnabled(true)
    }

    private fun setButtonsEnabled(enable: Boolean) {
        option1.isEnabled = enable
        option2.isEnabled = enable
        option3.isEnabled = enable
        option4.isEnabled = enable
    }

    private fun Button.setColorRed() {
        this.backgroundTintList = ColorStateList.valueOf(getColor(R.color.colorRed))
    }

    private fun Button.setColorGreen() {
        this.backgroundTintList = ColorStateList.valueOf(getColor(R.color.colorGreen))
    }

    private fun Button.setColorDarkBlue() {
        this.backgroundTintList = ColorStateList.valueOf(getColor(R.color.colorDarkBlue))
    }
}