package com.example.meuprimeiroapp

data class Question(
    val questionText: String,
    val optionsList: List<String>,
    val correctAnswerId: Int,
)